# CS:GO Player Graph Embeddings

Professional CS:GO presents a vast range of interesting problems in deep learning. At Sportsflare we build models
to predict the likelihood of teams winning in a CS:GO match to help our users pick the right team to back.

Most predictive modelling in this space is logistic regression based, and usually in the form of `y ~ X`, where y is the 
probability of a team winning `y`, given some input features `X`. The major drawback here is deciding on what 
features to use in `X`, and how to represent them numerically. Often these input features are aggregated in crude ways, 
such as using an [ELO ranking system](https://en.wikipedia.org/wiki/Elo_rating_system). It gets worse when players are
just represented by a list of their kills, deaths and assists. Not only does this mean that amateur players are over-powered 
in low ranks and appear better than they really are, but this also does not take into account the nuances
of the game, for example, a player's position in the team. 

This post addresses a simple deep learning technique that we use to boost our model performance by learning to 
summarize the data hidden in the network of connections between players / entities. 

Specifically, we will see how effective using graph embeddings can be for a range of tasks related to CS:GO analytics and prediction, with
[transfer learning](https://en.wikipedia.org/wiki/Transfer_learning).

## Learning Good Representations of Data 

Deep Learning techniques excel at learning detailed [representations](https://en.wikipedia.org/wiki/Feature_learning) of unstructured, and highly dimensional data. 
For instance in computer vision, dense feature representations of images (3D array of pixels) obtained from being run 
through a trained convolutional network have shown to be very useful input features `X` in a large range of other 
vision tasks. 

In natural language processing [Word2vec](https://en.wikipedia.org/wiki/Word2vec) easily illustrates this property (although it is not exclusive to 
deep learning). More recently eLMo, CoVe, and now BERT have shown learned contextual dense vectors of a word used 
for any context. So far, this contextual vector is only a property of deep learning models. 

These [embeddings](https://en.wikipedia.org/wiki/Embedding) have provided a significant boost to the performance of models that utilize them, in part because they 
inject further useful information obtained through unsupervised learning on a large amount of cheap data.

If you have followed along so far, I'm sure you are wondering how this technique can be applied to CS:GO players and
CS:GO in general. Unfortunately we don't have a data structure that really suits the priors for some of these deep learning 
building blocks right off the bat, such as images which are well suited to convolution, since pixels are locally 
correlated. However, we just have to be a bit more creative with our data structure.

The question is now: Can we find a way where we can represent CS:GO data such that we reflect the dynamics of the system,
but also adhere to the assumptions of these deep learning blocks / layers (i.e. recurrent networks for sequential data)?
The answer is yes, we can build a graph. In fact, we can build many types of graphs.       

## Building the Graph

At a basic level, a [graph](https://en.wikipedia.org/wiki/Graph_(abstract_data_type)) consists of nodes (vertices) and edges between nodes, where edges and nodes can represent 
anything to describe a system. In a social network, a node represents a person and an edge describes an interaction 
between two people. 

### CS:GO Graphs

In professional CS:GO, we could make nodes represent teams, and edges that connect two teams if 
they have played against each other. This is an extremely simple example, in which both nodes and edges have no 
associated attributes, there are no directed connections, and there is no concept of time.

![Network graph 1](https://i.imgur.com/kgH35fX.png)

We could add one more level of complexity by making nodes [directed](https://en.wikipedia.org/wiki/Directed_graph) from the loser to the winner, allowing us to 
roughly identify "good" teams by looking at nodes (teams) with a large number of inbound connections, and small number 
of outbound nodes. However, this type of graph does not capture the weighting of edges, for instance we may want to weight a 
directed edge by score difference to signify a close match, or an annihilation.

![Network graph 2](https://i.imgur.com/oFv075h.png)

At a complex level, we could use a graph structure to represent an entire CS:GO game, in which players are nodes and 
the edges are the interactions between them. For instance node A and node B are connected through a weighted and 
directed edge containing the associated information about that interaction. If player A killed player B, then a `kill` 
type edge with `timestamp`, `weapon`, `assailant_coordinates`, `victim_coordinates` and `damage _dealt` attributes 
would be created. This, more complex network analysis, is saved for a later post.

![Network graph 3](https://i.imgur.com/57Ow65c.png)

### Our Graph

As this is the first of our technical blogs, we will start with a less challenging network to conceptualize and 
apply machine learning algorithms to. Consider a graph of players that are connected by an edge if a they get a better 
`K/D` ratio than an opposing player for a given game. This means that the graph is multi-edged, directed, unweighted, 
non-temporal, and contains only one node and edge `type`, resulting in the following diagram. 

![Network graph 4](https://i.imgur.com/2eRh9za.png)

With that in mind, let's just step back and consider *just how little information we are working with*.

Some pythonic pseudo-code to generate this graph is as follows:

```Python
nodes = {player: idx for idx, player in enumerate(all_players)}

edges = []

for game in games:
    for t1_player in game['team_1']:
        for t2_player in game['team_2']:
            if t1_player['kd'] > t2_player['kd']:
                new_edge = {
                    "sender": nodes[t2_player['name']], 
                    "receiver": nodes[t1_player['name']]
                }
                edges.append(new_edge)
            elif t1_player['kd'] < t2_player['kd']:
                new_edge = {
                    "sender": nodes[t1_player['name']], 
                    "receiver": nodes[t2_player['name']]
                }
                edges.append(new_edge)
```

So that's fairly straightforward. Now that our graph is created we can get into the fun stuff.

## Machine Learning Algorithm

Now that we have a graph data structure, the challenge is to shape this data structure into a format that can be used by 
our machine learning. The algorithm [Node2vec](https://cs.stanford.edu/~jure/pubs/node2vec-kdd16.pdf) (N2V) is a way of 
representing a node's global position in a graph, given the various *contexts* a node may appear in. 
This technique is heavily inspired by [word2vec](https://en.wikipedia.org/wiki/Word2vec). 

For this example, we use a modified version N2V, where we generate contexts of a node by stochastically walking 
through the network given our starting node for a given number of steps. Our method differs from the standard N2V in 
that we use a 1D convolutional layer, to analyse the sequential and locally-connected aspects of the context. 
The model is trained on the task of predicting the starting node given the context sequence we generated, 
where the origin point is omitted.  

### Random Walk through Graph

Let **`A`** be the network adjacency matrix, in which all directed edges between two nodes (`i` and `j`) have been 
aggregated via summation **`A[i][j] = ∑ e{i->j}`**, where `e{i->j}` is an edge connecting sender `i` to receiver `j`. 

We can obtain the random walk results by a first-order Markov Chain, where the transition matrix **`T`** is just
the adjacency matrix **`A`**, normalized row-wise to the range `[0, 1]`.

Starting with origin node `o`, we can obtain our walk sequence `nodes` by the following python pseudocode:
```Python
nodes = [origin]

for t in range(length_of_walk):
    new_node = multinomial_sample(T[node[t]])  # sample next node from transition probs 
    nodes.append(new_node)
```

Visually, the first two steps in our random walk through the graph could be displayed:

![CSGO random walk](https://i.imgur.com/vDkkj98.png)

### Neural Network

The model architecture is described in the diagram and code snippets below.

![CSGO Neural Network](https://i.imgur.com/Xw0YzOo.png)

I didn't mention previously but the player "vocabulary" for this task is rather small, which allows us to get 
away with using softmax prediction. As the dimensionality grows, we could employ noise contrastive estimation 
to combat the reduction in speed.

For those using Pytorch the forward pass could be achieved using something like:
```Python
import torch.nn as nn

class N2V(nn.Module):
    def __init__(self):
        super(N2V, self).__init__()
        self.player_embedding = nn.Embedding()
        self.conv1d = nn.Conv1d(in_channels, out_channels, kernel_size)
        self.final = nn.Linear(, NUM_PLAYERS)

    def forward(input_players):
        embedded_players = self.player_embedding(input_players)
        conv_out = self.conv1d(embedded_players).view(BATCH_SIZE, -1)
        conv_out = nn.ELU()(conv_out)
        output = self.final(conv_out)
        output = nn.LogSoftmax(dim=1)()

        return output
```               

In Keras you could do the following for the forward pass:
```Python
from keras.models import Sequential
from keras.layers import Conv1D, Dense, Embedding

model = ([
    Embedding(),
    Conv1D(activation='elu'),
    Flatten(),
    Dense(activation='softmax')
 ]) 
```

## The Results (Magic)

After training the model using 100 biased random walks of length 10 for each node (player) for a few epochs,
we have a meaningfully organised CS:GO player vector space.

### How good are our learned representations?
To answer this, we turn to visualizing the player embeddings in 2D with TSNE.
If our representations are good, we will notice certain themes appearing in the organized player 
positions.

![TSNE Plot](https://i.imgur.com/vr2jScF.jpg)

The view of the *global space* is quite hard to examine, due to density of labels. 
To get a better picture of what's going on, let's drill down into two areas of interest: 
the `Australian neighbourhood`, and the `Top tier neighbourhood`.

**Australian Neighbourhood**

![Australia](https://i.imgur.com/PgSSFvu.jpg)

It's remarkable that even working with such little data, the model has learned to put almost 
exclusively Australian players in this part of the vector space.

**Top Tier Neighbourhood**

![Top Tier](https://i.imgur.com/ok6TaoK.jpg)

This part of the embedding space will be interesting to those who are familiar with pro CS:GO. 
Not only has it organised all "top tier" players into a neighbourhood, but it has also put
players in similar teams often very close to each other. For instance, the `Cloud 9` (and ex) players
`n0thing`, `freakazoid` and `stewie2K` are overlapping in this graph, illustrating how close their
embeddings are to each other. 

We can also search for similar players directly by the cosine distance. 
Here is an example of detected similar players, when searching for similar players to `friberg`.

![Nearest neighbour](https://i.imgur.com/C1phfX5.jpg)

I don't know enough CS:GO to properly make sense of these results, however all of these players are tier 1 
and the most similar player `Xizt` played many games alongside him in NiP.

The following sections also help us understand the information our learned representations are.

### Generalizing to Team, Game and Tournament Embeddings

Because `teams` consist of `players` we can actually aggregate the embeddings of players who comprise a team,
yielding a `team embedding` vector. This aggregation is simply achieved by summation. 

As `games` and `tournaments` ultimately consist of `players` we can extend this framework to yield 
game and tournament embedding vectors. 

Searching through `team`, `game` and `tournament` embeddings for a vector's closest match works extremely well.
Here is an example of a tournament, where the effect is the most pronounced.

![Aus Tournament](https://i.imgur.com/SZSkA2P.jpg)

Searching for the most similar tournaments to an Australian tournament yields (surprise) other Australian / NZ
tournaments.

### Tier Predictions

Because our embeddings now have an implicit understanding of where players fit in the global scale
we can use them as features to fuel predictive tasks about players.

We gave the vectors a small labelled dataset of 100 "tier 1" player embeddings, and 100 "low quality" player embeddings
as negative examples. We learned a simple new model mapping the embedding space to the probability of a player
being "tier 1", given only their embedding vector. Even with very limited training data, we managed to learn a 
99% accurate model tested on a hold-out set of 50 positive and negative samples.

Our `tier` model can now be used to tag players, teams, games and even tournaments with a tier rating.

Here is an example of rating two random tournaments automatically. We examine both a **tier 1** 
and a **tier 5** tournament, and show a random sample of some of the players.

![Tier 1 tourn](https://i.imgur.com/ycWnY1q.jpg)

![Tier 5 tourn](https://i.imgur.com/AsgT3z0.jpg)

What's interesting is to think of the other things we may be able to predict given these embeddings.
We tested using only the embeddings of two teams to predict the winner of a game, and yielded a 60% 
accuracy rate. This is really impressive as our main model is only a few % better, although it has
many features and a large amount of sequential input data.

### Embeddings for Other Models

This methodology (or similar) can be used to boost our main predictive Sportsflare model, 
as it helps contextualize players before it starts looking at the sequences of their performance. 
We have been able to exploit the rich information held in the network of connections between players and
other entities, and inject this into a model which cannot otherwise capture this data. 

## Conclusion

We learned a bit about using some basic deep learning to generate "summaries" of a CS:GO player network.

We saw how effective using graph embeddings can be for a range of tasks related to CS:GO analytics
and prediction, on a restricted set of input information, and how we can lift the performance of other 
predictive models by using these player representations for transfer learning.

Up next our team is exploring graph neural networks for in-game events, which is a far less trivial network,
and adding similar players, teams, games, tournaments and tiers as a new feature in our web application.
